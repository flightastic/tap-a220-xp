--AUTHOR: TAP DESIGNS--

--DATAREFS--
cold_start = find_dataref("sim/operation/prefs/startup_running")
battery = find_dataref("sim/cockpit/electrical/battery_on")


--FUNCTIONS--
function flight_start()
  if cold_start == false then
    battery = false
  end
end
