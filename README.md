TAP Design Group A220
X-Plane 11 Version Beta
Copyright: All downloaders shall be bounded by the GNU Public license V3. 
All rights reserved to TAP Designs 2020
Installation: Copy and paste the TAPA220-300X folder into you aircraft directory in XP11 main root folder
Contributers: FBI914 = Programming, Noduran = Front Gear, TD = Flight Model, LucasGC = Engines, The Best Durian = Cabin and Pedastol, 
Omegaman = Textures, Sxribe = Github and Gitlab management, Dark of Nova = Overhead Panel, TheArtfulPlonker = Everything else,